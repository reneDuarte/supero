/**
 * criar um modulo CadastroTarefa, injetando uma depedencia ngMaterial
 */

/**
 * scrol em table
 */
angular.module('CadastroTarefa', ['ngMaterial']);

/**
 * criar um controller no modulo CadastroTarefa passando como depedencia $scope, $http, $timeout
 * $scope: O escopo � a parte de liga��o entre o HTML (view) e o JavaScript (controller).
 * $http:  Faz uma solicita��o ao servidor e retorna uma resposta. Possue alguns metodos que utilizei algumas no projeto
 *          .delete()
 *          .get()
 *          .head()
 *          .jsonp()
 *          .patch()
 *          .post()
 *          .put()
 *
 * $timeout: Atraso em milissegundos na excecu��o de um metodo, utilizado no metodo mostrarMSG()
 *          ex:
 *          $timeout(function () {
     *          delete $scope.message;
 *          }, 2000)
 *
 *
 */
angular.module('CadastroTarefa').controller('CadastroTarefaCtrl', function ($scope, $http, $timeout) {
    $scope.app = "CadastroTarefa";
    $scope.listaTarefa = [];
    $scope.objListaTarefa = {};
    $scope.tarefa = {};

    $scope.listar  = function (forcaBusca) {
        if (!$scope.objListaTarefa.id || forcaBusca) {
            var id = prompt("Digite o ID da lista.");

            if (id) {
                $scope.objListaTarefa = {"id": id};
            } else {
                mostrarMSG("ID não informado.");
                return;
            }
        }
        $http.post('/rest/comando/listar' ,  $scope.objListaTarefa).then(
            function (request) {
                $scope.listaTarefa = request.data.customerList;
                if(!$scope.listaTarefa){
                    $scope.listaTarefa = [];
                }
            },
            function (request) {
                /**
                 * Mostra erro
                 * @see mostrarMSG
                 */
                mostrarMSG("Aconteceu um problema: " + request.data.erro);
            }
        );

    };


    /**
     * Busca Persistente por id
     *
     * @param tarefa.id
     */
    $scope.getForId = function (tarefa) {

        $http.get('/rest/comando/buscar/'+ NOME_CLASSE +'@' + tarefa.id).then(
            function (request) {
                /**
                 * recupera bean do retorno;
                 */
                $scope.tarefa = request.data.bean;

            },
            function (request) {
                /**
                 * Mostra erro
                 * @see mostrarMSG
                 */
                mostrarMSG("Aconteceu um problema: " + request.data.erro)

            }
        )
    };

    $scope.cancelar = function () {
        if ($scope.tarefa) {

            delete $scope.tarefa;
        }
        $scope.tarefa = {};
    };


    $scope.mostrarError = function (ele) {

        var mask = ele.$$attr.uiMask;
        if (ele.$invalid) {
            var campo = ele.$$attr.placeholder;
            var mask = ele.$$attr.uiMask;

            var msg = 'Erro para validar o campo;' + campo;
            if (mask)
                msg += "\nFavor digitar o valor no formato " + mask;

            mostrarMSG(msg);
            ele.$$element[0].focus();

            return ele.$invalid;
        }

    };

    $scope.deletar = function (tarefa) {
        if (confirm('deseja deletar o tarefa ' + tarefa.descricao)) {
            $http.delete('/rest/comando/delete/'+ NOME_CLASSE +'@' + tarefa.id).then(
                function (request) {
                    if (request.data.sucesso) {

                        $scope.listaTarefa = $scope.listaTarefa.filter(function (obj) {
                            if (tarefa.id != obj.id) return obj;
                        });

                        if ($scope.tarefa)
                            $scope.cancelar();

                    } else {
                        /**
                         * Mostra erro
                         * @see mostrarMSG
                         */
                        mostrarMSG("Aconteceu um problema: " + request.data.erro);
                    }
                },
                function (request) {
                    /**
                     * Mostra erro
                     * @see mostrarMSG
                     */
                    mostrarMSG("Aconteceu um problema: " + request.data.erro);

                }
            )
        }

    };

    $scope.editar = function (tarefa) {

        $http.put('/rest/comando/editar', tarefa).then(
            function (request) {

                tarefa = request.data.bean;
                if (request.data.sucesso) {

                    $scope.listaTarefa.some(function (el, i) {
                        if (el.id == tarefa.id) {
                            $scope.listaTarefa.splice(i, 1, angular.copy(tarefa));
                            return true;
                        }
                    });

                    $scope.cancelar();

                } else {
                    /**
                     * Mostra erro
                     * @see mostrarMSG
                     */
                    mostrarMSG("Aconteceu um problema: " + request.data.erro);

                    $scope.cancelar();
                }
            },
            function (request) {
                /**
                 * Mostra erro
                 * @see mostrarMSG
                 */
                mostrarMSG("Aconteceu um problema: " + request.data.erro);
                $scope.cancelar();
            }
        )

    };

    $scope.salvar = function (tarefa, naoValidarCampos) {
        var erro = false;
        /**
         * Imnpede a verifica��o de erro de preenchimento de campo
         */
        if (!naoValidarCampos) {
            if (!erro)
                erro = $scope.mostrarError(this.frmTarefa.titulo);
            if (!erro)
                erro = $scope.mostrarError(this.frmTarefa.descricao);
        }

        if (!erro) {
            if(!tarefa.listaTarefas){
                tarefa.listaTarefas = $scope.objListaTarefa
            }
            if (!tarefa.id)
                $scope.adicionar(tarefa);
            else
                $scope.editar(tarefa);
        }
    };

    $scope.trocarStatus = function (tarefa) {
        tarefa.concluido = !tarefa.concluido;
        $scope.salvar(tarefa, true);
        $scope.listar();
    };

    $scope.novaLista = function(){
        window.location.reload();
    }

    $scope.adicionar = function (tarefa) {

        $http.post('/rest/comando/salvar', tarefa).then(
            function (request) {

                if (request.data.sucesso) {
                    tarefa = request.data.bean;

                    $scope.listaTarefa.push(angular.copy(tarefa));
                    $scope.objListaTarefa = tarefa.listaTarefas;
                    $scope.cancelar();

                } else {
                    /**
                     * Mostra erro
                     * @see mostrarMSG
                     */
                    mostrarMSG("Aconteceu um problema: " + request.data.erro)
                }

            },
            function (request) {
                /**
                 * Mostra erro
                 * @see mostrarMSG
                 */
                mostrarMSG("Aconteceu um problema: " + request.data.erro)

            }
        )


    };

    function mostrarMSG(msg) {

        $scope.message = msg;

        $timeout(function () {
            delete $scope.message;
        }, 2000)
    }


    $scope.ordenarPor = function (campo) {

        $scope.criterio = campo;
        $scope.ordem = !$scope.ordem;

    };



});

criarUiMask('CadastroTarefa');