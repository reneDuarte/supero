package br.com.supero.rotinas;


import br.com.supero.util.HibernateUltil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Classe quem implementa o ServletContextListener que vista na hora de subir a aplicação e finalizar
 * para funcinar tem que ter anotação @WebListener ou maperar no web.xml
 */
//@WebListener
public class GerarBanco implements ServletContextListener {

    /**
     * Metodo chamado na hora de subir a aplecação
     * Criar as tabela no banco de dados com o hibernate
     *
     */
    private final HibernateUltil hibernateUltil;

    private GerarBanco(HibernateUltil hibernateUltil) {
        this.hibernateUltil = hibernateUltil;
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {

        /**
         * Criar instâcia do HibernateUltil para poder iniciar as variáveis finais
         * @see HibernateUltil#SESSION_FACTORY
         */
        HibernateUltil hibernateUltil = new HibernateUltil();
        hibernateUltil.getSessionFactory();
    }

    /**
     * metodo chamado na hora de terminar a aplicação
     *
     * @param arg0
     */
    @Override
    public void contextDestroyed(ServletContextEvent arg0) {

        //TODO fechar todas as Trazações
        /**
         * percebir que quado eu parava a aplicaçao esse metodo era chamado mais a aplicaçao continuava a roda
         * resolvi asimm
         * // TODO: 07/06/2018  análizar porque isso ocorre com a ide ItelliJ
         */
        System.exit(0);

    }
}
