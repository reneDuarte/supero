package br.com.supero.rest;

import br.com.supero.bean.Bean;
import br.com.supero.bean.ListaTarefas;
import br.com.supero.bean.Tarefa;
import br.com.supero.dao.Dao;
import br.com.supero.service.ServiceTarefa;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author rene.duarte
 * <p>
 * metodos utiizados
 * '@POST'
 * '@DELETE'
 * '@PUT'
 * '@GET'
 * <p>
 * tomei essa desição para demostrar sus funcionalidades
 */
@Path("/comando")
public class TarefaWS {

    /**
     * rest salvar com as seguintes configuração:
     * POST
     * Path("/salvar")
     * Consumes(MediaType.APPLICATION_JSON)
     * Produces(MediaType.APPLICATION_JSON)
     *
     * @param objTarefa
     * @return Retorno#fromJsomString()
     */

    @POST
    @Path("/salvar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Retorno salvar(Tarefa objTarefa) {
         return salvarTarefa(objTarefa);
    }

    /**
     * rest listar com as seguintes configuração:
     * POST
     * Path("/listar")
     * Consumes(MediaType.APPLICATION_JSON)
     * Produces(MediaType.APPLICATION_JSON)
     *
     * @return Retorno
     * @see Retorno#fromJsomString()
     */
    @POST
    @Path("/listar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Retorno listar(ListaTarefas listaTarefas) {
        Retorno retorno = new Retorno();
        try {

            ServiceTarefa serviceTarefa = new ServiceTarefa();
            List lista = serviceTarefa.listarTarefas(listaTarefas);

            retorno.setCustomerList(lista);

        } catch (Exception e) {
            /**
             * configura o objeto de retorno do rest com a mensagem de erro
             */
            retorno.setErro(e.getMessage());
        }

        return retorno;
    }

    /**
     * rest deletar com as seguintes configuracao:
     * DELETE
     * Path("/delete/{nomeClasseID}")
     * Produces(MediaType.APPLICATION_JSON)
     *
     * @param nomeClasseID (nome da classe e id EX:suaClasse@id)
     * @return Object
     */
    @DELETE
    @Path("/delete/{nomeClasseID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Retorno deletar(@PathParam("nomeClasseID") String nomeClasseID) {
        Retorno retorno = new Retorno();
        try {
            /**
             * verifica parametro inválido
             */
            if (nomeClasseID == null || "".equals(nomeClasseID) || !nomeClasseID.contains("@"))
                throw new Exception("Nome Classe e ou id não informado.");

            String[] dados = nomeClasseID.split("@");
            String nomeClasse = dados[0];
            Integer id = Integer.valueOf(dados[1]);

            Tarefa objAtiv = (Tarefa) Class.forName(nomeClasse).newInstance();
            objAtiv.setId(id);

            ServiceTarefa serviceTarefa = new ServiceTarefa();
            serviceTarefa.deletar(objAtiv);

            /**
             * configura o objeto de retorno do rest
             */
            retorno.setCustomer(objAtiv);
            retorno.setSucesso(true);


        } catch (Exception e) {
            /**
             * configura o objeto de retorno do rest com a mensagem de erro
             */
            retorno.setErro(e.getMessage());
        }

        return retorno;
    }

    /**
     * rest editar com as seguintes configuração:
     * PUT
     * Path("/editar")
     * Consumes(MediaType.APPLICATION_JSON)
     * Produces(MediaType.APPLICATION_JSON)
     *
     * @param objTarefa
     * @return Object
     * @see Retorno#fromJsomString()
     */
    @PUT
    @Path("/editar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Retorno editar(Tarefa objTarefa) {

        return salvarTarefa(objTarefa);
    }

    private Retorno salvarTarefa(Tarefa objTarefa) {
        Retorno retorno = new Retorno();
        try {
            ServiceTarefa serviceTarefa = new ServiceTarefa();
            serviceTarefa.salvar(objTarefa);

            /**
             * configura o objeto de retorno do rest
             */
            retorno.setCustomer(objTarefa);
            retorno.setSucesso(true);
        } catch (Exception e) {
            /**
             * configura o objeto de retorno do rest com a mensagem de erro
             */
            retorno.setErro(e.getMessage());
        }
        return  retorno;
    }

    /**
     * rest buscar por id com as seguintes configuração
     * GET
     * Path("/buscar/{nomeClasseID}")
     * Consumes(MediaType.APPLICATION_JSON)
     * Produces(MediaType.APPLICATION_JSON)
     *
     * @param nomeClasseID (nome da classe e id EX:suaClasse@id)
     * @return Retorno#fromJsomString()
     */
    @GET
    @Path("/buscar/{nomeClasseID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object getForId(@PathParam("nomeClasseID") String nomeClasseID) {
        Retorno retorno = new Retorno();
        try {

            if (nomeClasseID == null || "".equals(nomeClasseID) || !nomeClasseID.contains("@"))
                throw new Exception("Nome Classe e ou id não informado.");

            String[] dados = nomeClasseID.split("@");

            String nomeClasse = dados[0];
            Integer id = Integer.valueOf(dados[1]);

            Dao dao = new Dao();
            Bean bean = dao.buscarId(nomeClasse, id);

            retorno.setCustomer(bean);
            retorno.setSucesso(true);

        } catch (Exception e) {
            /**
             * configura o objeto de retorno do rest com a mensagem de erro
             */
            retorno.setErro(e.getMessage());

        }

        return retorno;
    }

}
