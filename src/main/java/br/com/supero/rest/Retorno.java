package br.com.supero.rest;


import br.com.supero.bean.Bean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.util.List;


/**
 * @author rene.duarte
 * <p>
 * Classe de retorno contendo as seguintes propriedades:
 * @see #customerList
 * @see #erro
 * @see #sucesso
 * @see #bean
 */
public class Retorno {
    /**
     * cria um objeto final do tipo {@link Gson} configurando o formato da data para "dd/MM/yyyy HH:mm:ss"
     * <p>
     * anotação {@link Expose} retira a propriedade na hora de gerar o json pelo {@link Gson}
     */
    @JsonIgnore
    @Expose
    private final Gson GSON = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
    /**
     * aceita só objeto que extend Bean
     */
    private List<? extends Bean> customerList;
    private String erro;
    private boolean sucesso = false;
    private Bean bean;

    /**
     * @return List
     */
    public List<? extends Bean> getCustomerList() {
        return customerList;
    }

    /**
     * @param customerList
     */
    public void setCustomerList(List<? extends Bean> customerList) {
        this.customerList = customerList;
    }

    /**
     * @return erro
     */

    public String getErro() {
        return erro;
    }

    /**
     * @param message
     */
    public void setErro(String message) {

        this.erro = message;
    }

    /**
     * @return boolean
     */
    public boolean isSucesso() {
        return sucesso;
    }

    /**
     * @param sucesso
     */
    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }

    /**
     * @param bean
     */
    public void setCustomer(Bean bean) {

        this.bean = bean;
    }

    /**
     * @return Bean
     */
    public Bean getBean() {

        return bean;
    }


    /**
     * converte this para json String
     *
     * @return String
     * @see Gson#toJson(Object)
     */
    public String fromJsomString() {

        String strJson = GSON.toJson(this);

        return strJson;

    }


}
