package br.com.supero.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tarefa")
public class Tarefa extends  Bean {
    /**
     * Não pode ser nulo no banco de dados
     */
    @Column(nullable = false)
    private String titulo;

    /**
     * Não pode ser nulo no banco de dados
     */
    @Column(nullable = false)
    private String descricao;

    /**
     * Campo verificado se a Atividade está finalizada
     */
    @Column(nullable = false)
    private Boolean concluido = false;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataCriacao;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataEdicao;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataConclusao;

    @ManyToOne
    @JoinColumn(nullable = false)
    private ListaTarefas listaTarefas;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getConcluido() {
        return concluido;
    }

    public void setConcluido(Boolean concluido) {
        this.concluido = concluido;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Date getDataEdicao() {
        return dataEdicao;
    }

    public void setDataEdicao(Date dataEdicao) {
        this.dataEdicao = dataEdicao;
    }

    public Date getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(Date dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public ListaTarefas getListaTarefas() {
        return listaTarefas;
    }

    public void setListaTarefas(ListaTarefas listaTarefas) {
        this.listaTarefas = listaTarefas;
    }
}
