package br.com.supero.exceptions;

/**
 * @author rene.duarte
 *
 * Exception especifca para regra de negócio:
 * Atividades de manutenção urgente não podem ser removidas, apenas
 * finalizadas;
 */
public class ExceptionsBuscarId extends Exception {

	private static final long serialVersionUID = 1L;

	public ExceptionsBuscarId(String erro) {
		super(erro);
	}
	
}
