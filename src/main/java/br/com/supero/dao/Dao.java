package br.com.supero.dao;


import br.com.supero.bean.Bean;
import br.com.supero.exceptions.ExceptionsBuscarId;
import br.com.supero.exceptions.ExceptionsCreateEntityManager;
import br.com.supero.util.HibernateUltil;
import com.sun.istack.internal.NotNull;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class Dao implements Idao {
    private Session session = null;

    public Dao() throws ExceptionsCreateEntityManager {
        iniciarSession();
    }

    @Override
    public Bean salvar(@NotNull Bean bean) throws Exception {

        try {
            iniciarSession();
            if (bean.getId() == null)
                session.persist(bean);
            else
                session.merge(bean);

            session.getTransaction().commit();


        } catch (Exception e) {
            session.getTransaction().rollback();
            throw e;

        } finally {
            fecharsession();
        }

        return bean;

    }


    @Override
    public Boolean deletar(@NotNull Bean bean) throws ClassNotFoundException, ExceptionsBuscarId, ExceptionsCreateEntityManager {
        try {
            iniciarSession();
            session.delete(bean);
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw e;
        } finally {
            fecharsession();
        }
    }

    public List listar(@NotNull Class<? extends Bean> zClass) {

        try {
            iniciarSession();
            Boolean execConsulta = true;
            List<Bean> listaTList = new ArrayList<>();

            String sql = "Select a from "+zClass.getSimpleName()+" as a";


            if (execConsulta){
                Query query = session.createQuery( sql, zClass);
                listaTList = query.getResultList();
            }

            return listaTList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fecharsession();
        }
        return null;
    }

    @Override
    public Session getSession() throws ExceptionsCreateEntityManager {
        return HibernateUltil.getSessionFactory().openSession();
    }

    @Override
    public Bean buscarId(@NotNull final String nomeClasse, @NotNull final Integer id) throws ExceptionsBuscarId {

        try {
            iniciarSession();
            Class<Bean> zClass = (Class<Bean>) Class.forName(nomeClasse);
            Bean bean = session.find(zClass, id);

            return bean;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionsBuscarId("Erro para buscar id: " + id + " da classe: " + nomeClasse);
        }finally {
            fecharsession();
        }
    }

    public void fecharsession() {
        if (session != null && session.isOpen()) {
            session.close();
        }
    }

    private void iniciarSession() throws ExceptionsCreateEntityManager {

        if(session == null){
            session  = getSession();
        }else if(!session.isOpen()){
            session = getSession();
        }

        if (!session.getTransaction().isActive()) {
            session.getTransaction().begin();
        }

    }
}
