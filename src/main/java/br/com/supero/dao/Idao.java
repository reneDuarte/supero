package br.com.supero.dao;



import br.com.supero.bean.Bean;
import br.com.supero.exceptions.ExceptionsBuscarId;
import br.com.supero.exceptions.ExceptionsCreateEntityManager;
import org.hibernate.Session;

import java.util.List;

public interface Idao {

    public Bean salvar(final Bean bean) throws Exception;
    public Boolean deletar(final Bean bean) throws ClassNotFoundException, ExceptionsBuscarId, ExceptionsCreateEntityManager;
    public List listar(final Class<? extends Bean> zClass);
    public Session getSession() throws ExceptionsCreateEntityManager;
    public Bean buscarId(final String nomeClasse, final Integer id) throws ClassNotFoundException, ExceptionsBuscarId;

}
