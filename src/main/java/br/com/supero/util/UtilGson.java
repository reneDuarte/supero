package br.com.supero.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

/**
 * Classe que configura o Gson {@link Gson}
 *
 * @author rene.duarte
 */
public class UtilGson {


    @Expose
    @JsonIgnore
    public static final Gson GSON = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();




}
