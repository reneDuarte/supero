package br.com.supero.util;

import br.com.supero.bean.Bean;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.reflections.Reflections;

import java.util.Set;

/**
 * Classe responsável por criar banco com Hibernate;
 * <p>
 * Classe  contendo as seguintes propriedades: *
 *
 * @see #PCK_BEAN
 * @see #CONFIGURATION
 * @see #SESSION_FACTORY
 */
public class HibernateUltil {
    // TODO: 07/06/2018 deixar configurável ou achar uma outras forma de mapear todas as classe de um pacote
    private static String PCK_BEAN = null;
    private static Configuration CONFIGURATION;

    /**
     * Criar o SESSION_FACTORY em uma variável final(imutável)
     */
    public static final SessionFactory SESSION_FACTORY = criarSessionFactory();

    /**
     * Criar tabelas  no banco de dados
     *
     * @return SessionFactory
     */
    private static final SessionFactory criarSessionFactory() {
        try {
            /**
             * Intância Configuration do Hibernate
             * @see Configuration
             */
            Configuration configuration = new Configuration();
            /**
             * Ler o arquivo de configuração do Hibernate hibernate.cfg.xml
             * @see Configuration#configure()
             */
            CONFIGURATION = configuration.configure();

            /**
             * Recupera a pacote bean que foi configurado no hibernate.cfg.xml
             */
            PCK_BEAN = CONFIGURATION.getProperty("bean");

            /**
             * printa um como arruma caso não ache o CONFIGURATION.getProperty("bean");
             */
            if (PCK_BEAN == null) {
                System.out.print("Favor adiciona a linha (  <property name=\"bean\">seu.pacote.bean</property> ) no arquivo hibernate.cfg.xml ");
            }

            /**
             * Percorre a lista de classes trazida peleo metodo getClasses() e adiciona no mapeamento do hibernate
             * @see this#getClasses()
             */
            for (Class c : getClasses()) {
                CONFIGURATION.addAnnotatedClass(c);
            }
            /**
             * cria a tabela no banco de dados
             */
            return CONFIGURATION.buildSessionFactory();

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;

    }

    /**
     * Percorre o pacote  PCK_BEAN e trás todas as classes  do tipo Bean
     *
     * @return Set<Class<? extends Bean>>
     */
    private static Set<Class<? extends Bean>> getClasses() {

        Reflections reflections = new Reflections(PCK_BEAN);
        Set<Class<? extends Bean>> classes = reflections.getSubTypesOf(Bean.class);

        return classes;
    }

    /**
     *
     * @return SESSION_FACTORY
     */

    public static SessionFactory getSessionFactory() {

        return SESSION_FACTORY;
    }

}
