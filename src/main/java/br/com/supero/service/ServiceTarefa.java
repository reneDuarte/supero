package br.com.supero.service;


import br.com.supero.bean.ListaTarefas;
import br.com.supero.bean.Tarefa;
import br.com.supero.dao.Dao;
import br.com.supero.exceptions.ExceptionsBuscarId;
import br.com.supero.exceptions.ExceptionsCreateEntityManager;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ServiceTarefa {

    public Tarefa salvar(Tarefa tarefa) throws Exception {

        Dao dao = new Dao();
        verificarListaTarefa(tarefa , dao);
        ajustarDatas(tarefa);
        dao.salvar(tarefa);

        return tarefa;
    }

    private void verificarListaTarefa(Tarefa tarefa , Dao dao) throws Exception {
        if(tarefa.getListaTarefas() == null || (tarefa.getListaTarefas() != null && tarefa.getListaTarefas().getId() == null)){
            ListaTarefas listaTarefas = new ListaTarefas();
            dao.salvar(listaTarefas);
            tarefa.setListaTarefas(listaTarefas);
        }else{
            dao.salvar(tarefa.getListaTarefas());
        }
    }

    private void ajustarDatas(Tarefa tarefa) {
        Date dataAtual  = new Date();
        if(tarefa.getConcluido()){
            tarefa.setDataConclusao(dataAtual);
        }else{
            tarefa.setDataConclusao(null);
        }

        if(tarefa.getId() == null){
            tarefa.setDataCriacao(dataAtual);
        }

        tarefa.setDataEdicao(dataAtual);

    }

    public void deletar(Tarefa objAtiv) throws ExceptionsBuscarId, ClassNotFoundException, ExceptionsCreateEntityManager {

        Dao dao = new Dao();
        objAtiv = (Tarefa) dao.buscarId(objAtiv.getClass().getName() ,objAtiv.getId() );

        dao.deletar(objAtiv);

    }

    public List<Tarefa> listarTarefas(ListaTarefas listaTarefas) throws ExceptionsCreateEntityManager {
        Dao dao = new Dao();
        Query query = dao.getSession().createQuery("from Tarefa where listaTarefas = " + listaTarefas.getId());

       List<Tarefa> l = query.getResultList();
       List ret = new ArrayList();
        /**
         * tratamento para recursividade de referencias
         */
       l.stream().forEach(obj ->{
           obj.setListaTarefas(null);
           ret.add(obj);
       });

       dao.fecharsession();
       return ret;
    }
}
